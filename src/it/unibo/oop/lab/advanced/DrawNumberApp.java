package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

/**
 */
public final class DrawNumberApp implements DrawNumberViewObserver {

    private int min = 0;
    private int max = 100;
    private int attempts = 10;
    private final DrawNumber model;
    private final DrawNumberView view;

    /**
     * 
     */
    public DrawNumberApp() {
        this.model = new DrawNumberImpl(min, max, attempts);
        this.view = new DrawNumberViewImpl();
        this.view.setObserver(this);
        this.view.start();
    }

    @Override
    public void newAttempt(final int n) {
        try {
            final DrawResult result = model.attempt(n);
            this.view.result(result);
        } catch (IllegalArgumentException e) {
            this.view.numberIncorrect();
        } catch (AttemptsLimitReachedException e) {
            view.limitsReached();
        }
    }

    @Override
    public void resetGame() {
        this.model.reset();
    }

    @Override
    public void quit() {
        System.exit(0);
    }

    private void importConfig(final String filename) throws IOException {
        try (BufferedReader rd = new BufferedReader(new InputStreamReader(ClassLoader.getSystemResourceAsStream(filename)))) {
            this.min = Integer.parseInt(stringFilterForImportConfig(rd.readLine()));
            this.max = Integer.parseInt(stringFilterForImportConfig(rd.readLine()));
            this.attempts = Integer.parseInt(stringFilterForImportConfig(rd.readLine()));
        } catch (FileNotFoundException e) {
            this.view.displayError("File not found");
        }
    }

    private String stringFilterForImportConfig(final String string) {
        StringTokenizer tokenizer = new StringTokenizer(string, ": ", false);
        String token = "";
        while (tokenizer.hasMoreTokens()) {
            token = tokenizer.nextToken();

        }
        return token;
    }

    /**
     * @param args
     *                 ignored
     * @throws IOException
     */
    public static void main(final String... args) throws IOException {
        new DrawNumberApp().importConfig("config.yml");
    }

}
