package it.unibo.oop.lab.mvc;

import java.util.ArrayList;
import java.util.List;

public class SimpleController implements Controller {

    private final List<String> list;
    private String nextstring;

    public SimpleController() {
        this.list = new ArrayList<>();
    }

    @Override
    public void setNextStringToPrint(final String string) {
        if (string == null) {
            throw new IllegalArgumentException();
        }
        this.nextstring = string;

    }

    @Override
    public String getNextStringToPrint() {
        return this.nextstring;
    }

    @Override
    public List<String> getPrintedStringsHistory() {
        return this.list;
    }

    @Override
    public void printCurrentString() {

        if (this.nextstring == null) {
            throw new IllegalStateException();
        }
        this.list.add(nextstring);
        System.out.print(nextstring);

    }

}
